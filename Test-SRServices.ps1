<#
.SYNOPSIS
    Skrypt sprawdza stan podanych us�ug na zadanych serwerach.

.DESCRIPTION
    Skrypt ��czy si� z kolejnymi serwerami i komunikuje stan wskazanych us�ug, 
    ewentualnie fakt �e danej us�ugi nie ma na serwerze, 
    je�li us�uga ma stan 'Stopped' skrypt mo�e podj�� pr�b� uruchomienia jej.

.PARAMETER ServerName
    Parametr okre�laj�cy nazwy serwer�w. Parametr wymagany.

.PARAMETER ServiceName
    Parametr okre�laj�cy nazwy us�ug kt�re chcemy sprawdzi�. Parametr wymagany.

.PARAMETER Log
    �cie�ka do katalogu w kt�rymym zapisany b�dzie log z wykonania skryptu. Parametr opcionalny.

.PARAMETER NotRun
    Prze��cznik, je�li wywo�amy skrypt z tym parametrem to skrypt nie b�dzie podejmowa� pr�by uruchomienia us�ug w stanie 'Stopped'. 

.PARAMETER NotFound
    Prze��cznik, je�li wywo�amy skrypt z tym parametrem to skrypt dodatkowo b�dzie wy�wietla� komunikaty
     odno�nie us�ug nieznalezionych na serwerze.

.INPUTS
    None

.OUTPUTS
    Plik logu (Test-SRService_2024-02-19_22.46.20.log) w formacie:

    [INFO] 2024-02-19_22.46.20 Log started
    [INFO] 2024-02-19_22.46.21 Checking: [SRV1]
    [INFO] 2024-02-19_22.46.21 [SRV1] Is online
    [INFO] 2024-02-19_22.46.21 [SRV1] ImportM is Running
    [INFO] 2024-02-19_22.46.21 [SRV1] Neur Import is Running
    [ERROR] 2024-02-19_22.46.21 [SRV1] Cannot find service: ReService
    [ERROR] 2024-02-19_22.46.21 [SRV1] Cannot find service: RAgent
    ...

.NOTES
  Version:        1.1
  Author:         Sebastian Cichonski
  Creation Date:  10.2023
  Projecturi:     https://gitlab.com/powershell1990849/test-service 
  
.EXAMPLE 
 W przyk�adach wywo�ania skryptu dla danych: 2 serwery, 4 us�ugi w tym 1 us�uga ze statusem 'Stopped'

  Test-SRServices.ps1 -ServerName SRV1, SRV2 -ServiceName ImportM, "Neur Import", ReService, RAgent -LogPath C:\ps_scripts -NotFound -NotRun

[INFO]  > Checking: [SRV1]
[INFO]  > [SRV1] Is online 
[INFO]  > [SRV1] ImportM is Running 
[ERROR] > [SRV1] Neur Import is Stopped 
[ERROR] > [SRV1] Cannot find service: ReService 
[ERROR] > [SRV1] Cannot find service: RAgent 

[INFO]  > Checking: [SRV2]
[INFO]  > [SRV2] Is online 
[ERROR] > [SRV2] Cannot find service: ImportM 
[ERROR] > [SRV2] Cannot find service: Neur Import 
[INFO]  > [SRV2] ReService is Running 
[INFO]  > [SRV2] RAgent is Running 

.EXAMPLE 

Test-SRServices.ps1 -ServerName SRV1, SRV2 -ServiceName ImportM, "Neur Import", ReService, RAgent -LogPath C:\ps_scripts -NotRun          

[INFO]  > Checking: [SRV1]
[INFO]  > [SRV1] Is online 
[INFO]  > [SRV1] ImportM is Running 
[ERROR] > [SRV1] Neur Import is Stopped 

[INFO]  > Checking: [SRV2]
[INFO]  > [SRV2] Is online 
[INFO]  > [SRV2] ReService is Running 
[INFO]  > [SRV2] RAgent is Running 

.EXAMPLE

Test-SRServices.ps1 -ServerName SRV1, SRV2 -ServiceName ImportM, "Neur Import", ReService, RAgent -LogPath C:\ps_scripts        

[INFO]  > Checking: [SRV1]
[INFO]  > [SRV1] Is online 
[INFO]  > [SRV1] ImportM is Running 
[ERROR] > [SRV1] Neur Import is Stopped 
[INFO]  > [SRV1] Trying to start a service: Neur Import
[SUCCESS]       > [SRV1] The service Neur Import was successfully started. 

[INFO]  > Checking: [SRV2]
[INFO]  > [SRV2] Is online 
[INFO]  > [SRV2] ReService is Running 
[INFO]  > [SRV2] RAgent is Running
#>


[CmdletBinding()]
Param(
    [Parameter(Mandatory)]
    [string[]]$ServerName,    

    [Parameter(Mandatory)]
    [string[]]$ServiceName,
 
    [alias("Log")]
    [ValidateScript({Test-Path $_ -PathType "Container"})]
    $LogPath,

    [switch]$NotRun,
    [switch]$NotFound
)

function IsRunning {
    param (
    [Parameter()]
    [string] $Computer,
    [string] $Service
    )
    if((Get-Service -ComputerName $Computer -Name $Service).Status -eq "Running") {
        return $true
    }
    else {
        return $false
    }
}

function Get-LongDate {
    Get-Date -Format "yyyy-MM-dd_HH.mm.ss"
}
function Write-Log {
    param (
        [Parameter()]
        [string]$Type,
        [string]$Value
    )
    if($Type -eq "Err") {
        Write-Host -ForegroundColor Red "[ERROR]`t> $Value "
        if($LogPath) {
            Add-Content -Path $Log  -Encoding Ascii -Value "[ERROR] $(Get-LongDate) $Value"
        }
    }
    if($Type -eq "Succ") {
        Write-Host -ForegroundColor green "[SUCCESS]`t> $Value "
        if($LogPath) {
            Add-Content -Path $Log  -Encoding Ascii -Value "[SUCCESS] $(Get-LongDate) $Value"
        }
    }
    if($Type -eq "Info") {
        Write-Host "[INFO]`t> $Value "
        if($LogPath) {
            Add-Content -Path $Log  -Encoding Ascii -Value "[INFO] $(Get-LongDate) $Value"
        }
    }
}

if($LogPath) {
    $LogDate = Get-LongDate
    $Log = "$LogPath\Test-SRService_$LogDate.log"
    if(Test-Path -Path $Log) {
        Clear-Content -Path $Log
    }
    Add-Content -Path $Log -Encoding Ascii -Value "[INFO] $(Get-LongDate) Log started"
}

foreach($Computer in $ServerName) {
    Write-Host
    Write-Log -Type "Info" -Value "Checking: [$Computer]"
    if(Test-Connection -ComputerName $Computer -Count 1 -Quiet) {
        Write-Log -Type "Info" -Value  "[$Computer] Is online"
        foreach($Service in $ServiceName) {
            if(Get-Service -ComputerName $Computer -Name $Service -ErrorAction SilentlyContinue) {
                if(IsRunning $Computer $Service) {
                    Write-Log -Type "Info" -Value "[$Computer] $service is Running"
                }
                else { 
                    Write-Log -Type "Err" -Value "[$Computer] $Service is Stopped"
                    if($NotRun -eq $false) {
                        Write-Log -Type "Info" -Value  "[$Computer] Trying to start a service: $Service"
                        Invoke-command -ComputerName $Computer -ScriptBlock { Start-Service -Name $using:Service }
                        if(IsRunning $Computer $Service) {
                            Write-Log -Type "Succ" -Value "[$Computer] The service $Service was successfully started."
                        }
                        else {
                            Write-Log -Type "Err" -Value "[$Computer] Filed to start service: $Service" 
                        }
                    }
                }
            }
            elseif($NotFound){
                Write-Log -Type "Err" -Value "[$Computer] Cannot find service: $Service"
            }
        }
    }
    else {
        Write-Log -Type "Err" -Value "[$Computer] is offline" 
    }
}