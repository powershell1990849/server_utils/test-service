

# Test-SRServices.ps1

## SYNOPSIS
Skrypt sprawdza stan podanych us�ug na zadanych serwerach.

## SYNTAX

```
Test-SRServices.ps1 [-ServerName] <String[]> [-ServiceName] <String[]> [[-LogPath] <Object>] [-NotRun]
 [-NotFound] [<CommonParameters>]
```

## DESCRIPTION
Skrypt ��czy si� z kolejnymi serwerami i komunikuje stan wskazanych us�ug, 
ewentualnie fakt �e danej us�ugi nie ma na serwerze, 
je�li us�uga ma stan 'Stopped' skrypt mo�e podj�� pr�b� uruchomienia jej.

## EXAMPLES
W przyk�adach wywo�ania skryptu dla danych: 2 serwery, 4 us�ugi w tym 1 us�uga ze statusem 'Stopped'
### EXAMPLE 1
```
Test-SRServices.ps1 -ServerName SRV1, SRV2 -ServiceName ImportM, "Neur Import", ReService, RAgent -LogPath C:\ps_scripts -NotFound -NotRun

```


 \[INFO\]  \> Checking: \[SRV1\]\
\[INFO\]  \> \[SRV1\] Is online \
\[INFO\]  \> \[SRV1\] ImportM is Running \
\[ERROR\] \> \[SRV1\] Neur Import is Stopped \
\[ERROR\] \> \[SRV1\] Cannot find service: ReService \
\[ERROR\] \> \[SRV1\] Cannot find service: RAgent \

\[INFO\]  \> Checking: \[SRV2\]\
\[INFO\]  \> \[SRV2\] Is online \
\[ERROR\] \> \[SRV2\] Cannot find service: ImportM \
\[ERROR\] \> \[SRV2\] Cannot find service: Neur Import \
\[INFO\]  \> \[SRV2\] ReService is Running \
\[INFO\]  \> \[SRV2\] RAgent is Running\

### EXAMPLE 2
```
Test-SRServices.ps1 -ServerName SRV1, SRV2 -ServiceName ImportM, "Neur Import", ReService, RAgent -LogPath C:\ps_scripts -NotRun
```

\[INFO\]  \> Checking: \[SRV1\]\
\[INFO\]  \> \[SRV1\] Is online \
\[INFO\]  \> \[SRV1\] ImportM is Running \
\[ERROR\] \> \[SRV1\] Neur Import is Stopped \

\[INFO\]  \> Checking: \[SRV2\]\
\[INFO\]  \> \[SRV2\] Is online \
\[INFO\]  \> \[SRV2\] ReService is Running\ 
\[INFO\]  \> \[SRV2\] RAgent is Running\

### EXAMPLE 3
```
Test-SRServices.ps1 -ServerName SRV1, SRV2 -ServiceName ImportM, "Neur Import", ReService, RAgent -LogPath C:\ps_scripts
```

\[INFO\]  \> Checking: \[SRV1\]\
\[INFO\]  \> \[SRV1\] Is online \
\[INFO\]  \> \[SRV1\] ImportM is Running \
\[ERROR\] \> \[SRV1\] Neur Import is Stopped \
\[INFO\]  \> \[SRV1\] Trying to start a service: Neur Import\
\[SUCCESS\]       \> \[SRV1\] The service Neur Import was successfully started. 

\[INFO\]  \> Checking: \[SRV2\]\
\[INFO\]  \> \[SRV2\] Is online \
\[INFO\]  \> \[SRV2\] ReService is Running \
\[INFO\]  \> \[SRV2\] RAgent is Running\

## PARAMETERS

### -ServerName
Parametr okre�laj�cy nazwy serwer�w.
Parametr wymagany.

```yaml
Type: String[]
Parameter Sets: (All)
Aliases:

Required: True
Position: 1
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -ServiceName
Parametr okre�laj�cy nazwy us�ug kt�re chcemy sprawdzi�.
Parametr wymagany.

```yaml
Type: String[]
Parameter Sets: (All)
Aliases:

Required: True
Position: 2
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -LogPath
�cie�ka do katalogu w kt�rymym zapisany b�dzie log z wykonania skryptu. Parametr opcionalny.

```yaml
Type: Object
Parameter Sets: (All)
Aliases: Log

Required: False
Position: 3
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -NotRun
Prze��cznik, je�li wywo�amy skrypt z tym parametrem to skrypt nie b�dzie podejmowa� pr�by uruchomienia us�ug w stanie 'Stopped'.

```yaml
Type: SwitchParameter
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: False
Accept pipeline input: False
Accept wildcard characters: False
```

### -NotFound
Prze��cznik, je�li wywo�amy skrypt z tym parametrem to skrypt dodatkowo b�dzie wy�wietla� komunikaty
     odno�nie us�ug nieznalezionych na serwerze.

```yaml
Type: SwitchParameter
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: False
Accept pipeline input: False
Accept wildcard characters: False
```

### CommonParameters
This cmdlet supports the common parameters: -Debug, -ErrorAction, -ErrorVariable, -InformationAction, -InformationVariable, -OutVariable, -OutBuffer, -PipelineVariable, -Verbose, -WarningAction, and -WarningVariable. For more information, see [about_CommonParameters](http://go.microsoft.com/fwlink/?LinkID=113216).

## INPUTS

### None
## OUTPUTS

 Plik logu (Test-SRService_2024-02-19_22.46.20.log) w formacie:\
 [INFO] 2024-02-19_22.46.20 Log started\
 [INFO] 2024-02-19_22.46.21 Checking: [SRV1]\
 [INFO] 2024-02-19_22.46.21 [SRV1] Is online\
 [INFO] 2024-02-19_22.46.21 [SRV1] ImportM is Running\
 [INFO] 2024-02-19_22.46.21 [SRV1] Neur Import is Running\
 [ERROR] 2024-02-19_22.46.21 [SRV1] Cannot find service: ReService\
 [ERROR] 2024-02-19_22.46.21 [SRV1] Cannot find service: RAgent\
 ...
## NOTES
Version:        1.1\
Author:         Sebastian Cichonski\
Creation Date:  10.2023\
Projecturi:     https://gitlab.com/powershell1990849/test-service

## RELATED LINKS
